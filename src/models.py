from nltk.lm.preprocessing import padded_everygram_pipeline
from nltk.lm import MLE


class LanguageModel:
    def __init__(self, tokenizer, size):
        self.tokens = None
        self.gender = None
        self.age = None
        self.tokenizer = tokenizer
        self.size = size
        self.model = None

    def generate_tokens(self, list_of_input_strings):
        token_list = []
        for post in list_of_input_strings:
            token_list.append(["<s>"]+self.tokenizer.tokenize(post)+["</s>"])
        self.tokens = token_list
        return self.tokens

    def generate_tokens_from_single(self, pd_entry):
        token_list = [["<s>"] + self.tokenizer.tokenize(pd_entry) + ["</s>"]]
        return token_list

    def set_gender(self, gender_list):
        self.gender = gender_list
        return self.gender

    def set_age(self, age_list):
        self.age = age_list
        return self.age

    def train_model(self):
        self.model = MLE(self.size)
        train_data, padded_sents = padded_everygram_pipeline(self.size, self.tokens)
        self.model.fit(train_data, padded_sents)
        return self.model
