from input_management import *
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
import os.path
import argparse
# from keras.models import Sequential

import sys

sys.path.append('../venv/lib/python3.7/site-packages')

"""Tokenizer used for turning words into tokens from the DataFrame"""


def tokenize(row):
    tokenizer = Tokenizer()
    return tokenizer.tokenize(row)


"""Converting text gender into binary: 1 = female, 0 = male"""


def bin_gender(row):
    if row == 'female':
        return "1"
    else:
        return "0"


"""Binning ages into age groups"""


def age_group(row):
    if 13 <= int(row) <= 17:
        return 1
    if 23 <= int(row) <= 27:
        return 2
    if 33 <= int(row) <= 37:
        return 3


def print_and_return(value):
    print(value)
    return value


arg_parser = argparse.ArgumentParser(description="Make predictions of personal data based on produced content")
arg_parser.add_argument('--force_reload',
                        dest='force_reload',
                        type=bool,
                        default=False,
                        help='Force reloading of the DataFrame even though pickle exists.',
                        choices=[True, False])
"""arg_parser.add_argument('--method',
                        dest='method',
                        type=str,
                        default="LogisticRegression",
                        help='Set method to be used for model creation and calculating accuracy',
                        choices=["LogisticRegression", "DeepLearning"])"""
arg_parser.add_argument('--sample_size',
                        dest='sample_size',
                        type=int,
                        default=10000,
                        help='Size of the sample to be split between train and test'
                        )
arg_parser.add_argument('--iterations',
                        dest='iterations',
                        type=int,
                        default=1,
                        help='Number of iterations (times the software is to be executed)')

args = arg_parser.parse_args()

if os.path.exists('../data/dataframe.pkl') and not args.force_reload:
    print("Reading pickle...")
    df = pd.read_pickle('../data/dataframe.pkl')
    print("Pickle read!")
else:
    if os.path.exists('../data/dataframe.pkl'):
        os.remove('../data/dataframe.pkl')
    print("Creating pickle... (that might take some time)")
    print("Reading files...")
    reader = FileReader("../data/blogs")
    reader.read_files()

    print("Parsing files...")
    parser = ContentParser(reader)
    parser.parse_files()
    documents = parser.post_list
    df = pd.DataFrame()

    for i in range(len(documents)):
        n = len(documents[i])
        temp_df = pd.DataFrame({
            'author_id': pd.Series(n * [parser.author_id[i]]),
            'gender': pd.Series(n * [parser.author_gender[i]]),
            'age': pd.Series(n * [parser.author_age[i]]),
            'content': pd.Series(documents[i])
        })
        df = df.append(temp_df)
    df['words'] = df['content'].apply(
        lambda x: len(tokenize(x))
    )

    df = df[~(df['words'] == 0)]

    df['gender'] = df['gender'].apply(
        lambda x: bin_gender(x)
    )

    df['age_group'] = df['age'].apply(
        lambda x: str(age_group(x))
    )
    df.to_pickle('../data/dataframe.pkl')
    print("Pickle created!")

sample_size = args.sample_size

for i in range(args.iterations):
    print("Iteration : {} out of {}".format(i+1, args.iterations))
    print("Creating random sample of size {}...".format(sample_size))
    df = df.sample(n=sample_size)
    print("Sample created!")

    print("Splitting into train and test...")
    train_df, test_df = train_test_split(
        df,
        test_size=0.3,
        # stratify=df['author_id'],
    )

    print("Vectorising...")
    vectoriser = CountVectorizer(analyzer='char_wb', ngram_range=(5, 5))
    vectoriser.fit(train_df['content'])

    vectoriser2 = CountVectorizer(analyzer='char_wb', ngram_range=(3, 5))
    vectoriser2.fit(train_df['content'])

    vectoriser3 = CountVectorizer(analyzer='char_wb', ngram_range=(9, 9))
    vectoriser3.fit(train_df['content'])

    results_dict = {
        'gender55': [],
        'age55': [],
        'gender35': [],
        'age35': [],
        'gender99': [],
        'age99': []
    }

    print("Running simulations for ngram_range=(5,5)...")

    print("Creating train and test vectors...")
    X_train = vectoriser.transform(train_df['content'])
    X_test = vectoriser.transform(test_df['content'])

    print("Running logistic regression for prediction of author's gender...")
    # Model for predicting gender
    model_gender = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_gender.fit(X_train, train_df['gender'])
    score = model_gender.score(X_test, test_df['gender'])
    results_dict['gender55'].append(score)
    print(score)

    print("Running logistic regression for prediction of author's age group...")
    # Model for predicting age group
    model_age = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_age.fit(X_train, train_df['age_group'])
    score = model_age.score(X_test, test_df['age_group'])
    results_dict['age55'].append(score)
    print(score)

    print("Running simulations for ngram_range=(3,5)...")

    print("Creating train and test vectors...")
    X_train = vectoriser2.transform(train_df['content'])
    X_test = vectoriser2.transform(test_df['content'])

    print("Running logistic regression for prediction of author's gender...")
    # Model for predicting gender
    model_gender = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_gender.fit(X_train, train_df['gender'])
    score = model_gender.score(X_test, test_df['gender'])
    results_dict['gender35'].append(score)
    print(score)

    print("Running logistic regression for prediction of author's age group...")
    # Model for predicting age group
    model_age = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_age.fit(X_train, train_df['age_group'])
    score = model_age.score(X_test, test_df['age_group'])
    results_dict['age35'].append(score)
    print(score)

    print("Running simulations for ngram_range=(9,9)...")

    print("Creating train and test vectors...")
    X_train = vectoriser3.transform(train_df['content'])
    X_test = vectoriser3.transform(test_df['content'])

    print("Running logistic regression for prediction of author's gender...")
    # Model for predicting gender
    model_gender = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_gender.fit(X_train, train_df['gender'])
    score = model_gender.score(X_test, test_df['gender'])
    results_dict['gender99'].append(score)
    print(score)

    print("Running logistic regression for prediction of author's age group...")
    # Model for predicting age group
    model_age = LogisticRegression(class_weight='balanced', dual=True, solver='liblinear')
    model_age.fit(X_train, train_df['age_group'])
    score = model_age.score(X_test, test_df['age_group'])
    results_dict['age99'].append(score)
    print(score)

    results_df = pd.DataFrame(results_dict)
    results_df.to_csv('../data/results.csv', index=False, mode='a', header=False)

print("Done!")
