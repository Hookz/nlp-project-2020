import os
import re
from nltk import word_tokenize


class FileReader:
    def __init__(self, directory):
        self.directory = os.path.abspath(directory) + "/"
        self.file_list = []
        self.file_path_list = []

    def read_files(self):
        file_dir_list = os.listdir(self.directory)
        self.file_list = file_dir_list
        file_dir_list = [self.directory + "/" + file for file in file_dir_list]
        self.file_path_list = file_dir_list
        return


class ContentParser:
    """This is the main content parser, it parses the files read in by the FileReader and passes them to the XMLParser
    for parsing XML arguments (post / date). Additionally info from file names is parsed (age and gender of author)"""
    def __init__(self, file_reader):
        self.file_reader = file_reader
        self.post_list = []
        self.author_gender = []
        self.author_age = []
        self.author_id = []

    def parse_files(self, size=19320):
        for file in self.file_reader.file_list[:size]:
            if file.split(".")[1] != "DS_Store":
                reader = open(self.file_reader.directory+file, "r", encoding='iso-8859-1')
                file_contents = XMLParser(reader.read()).find_all_posts()
                self.post_list += [file_contents.post]
                self.author_age += [file.split(".")[2]]
                self.author_gender += [file.split(".")[1]]
                self.author_id += [file.split(".")[0]]
        return


class XMLParser:
    """This is a custom XML Parser. It was implemented because the standard input wasn't being parsed correctly
    by the ElementTree parser"""
    def __init__(self, file_contents):
        self.file_contents = file_contents
        self.post = []
        self.date = []

    def find_all_posts(self):
        post = []
        date = []
        opening_post = [m.end() for m in re.finditer("<post>", self.file_contents)]
        closing_post = [m.start() for m in re.finditer("</post>", self.file_contents)]
        opening_date = [m.end() for m in re.finditer("<date>", self.file_contents)]
        closing_date = [m.start() for m in re.finditer("</date>", self.file_contents)]
        if len(opening_post) == len(closing_post) == len(opening_date) == len(closing_date):
            for i in range(len(opening_post)):
                post.append(self.file_contents[opening_post[i]:closing_post[i]])
                date.append(self.file_contents[opening_date[i]:closing_date[i]])
        self.post = post
        self.date = date
        return self


class Tokenizer:
    def __init__(self):
        self.tokens = None

    def tokenize(self, string):
        self.tokens = word_tokenize(string)
        return self.tokens
