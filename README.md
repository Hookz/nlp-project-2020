# NLP Project 2020

This project is about building a linear regression model for predicting age and
gender of people based on their writing style. To accomplish this, the [Blogger.com
2004 Blog Authorship Corpus](https://u.cs.biu.ac.il/~koppel/BlogCorpus.htm) was used as a data source, and various Python libraries were equipped to build the model.

## Python requirements
The project was compiled and run using Python 3.7.4. All required packages are listed in [requirements.txt](requirements.txt).

## Methods
This software uses Linear Regression from *scikit-learn* and various tokenisers and word vectorisers from other packages. All data is saved in a *pandas* DataFrame.

There are three kinds of word vectorisers used: one with 5-grams, one with 3-to-5-grams, and one with 9-grams. All three vectorisers are run with two linear regression models: one for age prediction and one for gender prediction. In total, there are 6 simulations run for each iteration of the software.

## How to run the software
The main executable file is [main.py](src/main.py). It loads all required scripts containing classes needed to run the simulations. An argument parser is employed to allow parametrisation. The example code below allows to change the default values (discussed in the section below) for sample size to 12345 and iterations to 10 (with force_reload left at default).
```shell
python3 main.py --sample_size 12345 --iterations 10
```

### Default setup
 * --force_reload False
 * --sample_size 10000
 * --iterations 1

The force reload is set to False by default, since it controls whether the pickle created for faster DataFrame loading should be destroyed (if found) and recreated instead of loaded. There are some benefits of choosing this option, eg. in a situation when the model was updated or new data was added.

The sample size is at 10 000 as it allows the sample to be more representative than a smaller one while also making it possible to run on a laptop, in situations when running it in a huge data lab on all of the data would be impossible (like in the case of my study).

The default of one iteration was a pure design choice. It does not affect the functioning of this software, but allows to run it quickly once without having to wait for a lot of results to appear before the program's execution stops.

### Results retainment

The software is set up to keep track of all results by saving them to a CSV file. At every successful completion of all six linear regression tasks, the outcomes are exported to CSV format and added to that file.

## Final remarks
This was an individual project, and as such was fully developed by @Hookz. Since this project has already been properly archived for submission (commit eb0c46d6), any changes to this project are welcome.

## License
This software is published under the [MIT License](LICENSE.md).
